# uufile example


```bash
#!/usr/bin/env bash

PATH=./node_modules/.bin:$PATH

### ### ###
# css
### ### ###

function uurun_add.css {
	sass css/style.scss:css/style.css
}

function uurun_add.css_live {
	sass --watch css/style.scss:css/style.css
}

### ### ###
# Browsersync
### ### ###

function uurun_add.watch {
	uurun_utils.notify "Browsersync"
	browser-sync start --server src --no-open --files="src/index.html, src/js/*.js, src/css/*.css"
}

### ### ###
# Deploy FTP
### ### ###

function uurun_add.upload {
	uurun_utils.notify "Start Deploy ..."
	lftp <<EOF
set ssl-allow false
open -u USERNAME,PASSWORD SERVER

mirror -R -e -v src /path/to/www

EOF
	
	uurun_utils.notify "Deployed by FTP"
}

### ### ###
# Deploy RSYNC
### ### ###

function deploy {
	rsync -Pavz --exclude=".git" --exclude-from=".gitignore" src /path/to/www
	notify-send "Deploy by SSH"
}

```